import { HttpException } from "./HttpException.js";

export class UserNotFoundException extends HttpException {
    constructor(data: string){
        super(404, `User ${data} was not found`);
    }
}