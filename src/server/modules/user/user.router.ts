/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error 
*/
import raw from "../../middleware/route.async.wrapper.js";
import express, { NextFunction, Request, Response } from "express";
import user_service from "./user.service.js";
import { UserNotFoundException } from "../../exceptions/UserNotFoundException.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER  - without wrapping it with "raw" function
//#region 
// router.post("/", async (req, res,next) => {
//    try{
//      const user = await user_model.create(req.body);
//      res.status(200).json(user);
//    }catch(err){
//       next(err)
//    }
// });
//#endregion

// CREATES A NEW USER
router.post("/", raw( async (req: Request, res: Response, next: NextFunction) => {
    const user = await user_service.createUser(req.body);
      if(!user) {
        next(new Error("User details are invalid"));
      }
      else res.status(200).json(user);
  })
);

// GET ALL USERS
router.get( "/",raw(async (req: Request, res: Response) => {
    const users = await user_service.getAllUsers();
    res.status(200).json(users);
  })  
);

// GETS A SINGLE USER
router.get("/:id",raw(async (req: Request, res: Response, next: NextFunction) => {
  const user = await user_service.getById(req.params.id);
    if (!user) {
      next(new UserNotFoundException(req.params.id));
    }
    else res.status(200).json(user);
  })
);

// UPDATES A SINGLE USER
router.put("/:id",raw(async (req: Request, res: Response, next: NextFunction) => {
  const user = await user_service.updateUser(req.params.id, req.body);
    if(!user) {
      next(new UserNotFoundException(req.params.id));
    }
    else {
      res.status(200).json(user);
    }
  })
);

// DELETES A USER
router.delete("/:id",raw(async (req: Request, res: Response, next: NextFunction) => {
    const user = await user_service.deleteUser(req.params.id);
    if (!user) {
      next(new UserNotFoundException(req.params.id));
    }
    else res.status(200).json(user);
  })
);

// PAGINATION
router.get("/paginate/:pageNum/:numOfUsers",raw(async (req: Request, res: Response) => {
    const users = await user_service.paginate(Number(req.params.pageNum), Number(req.params.numOfUsers));
    if (!users) return res.status(404).json({ status: "No user found." });
    res.status(200).json(users);
  })
);


export default router;