import user_model, { IDbUser } from "./user.model.js";
import mongoose from "mongoose";
import { ICreateUser, IUpdateUser } from "./user.interface.js";
import { CREATE_USER, UPDATE_USER, validateUserData } from "./user.validation.js";


class UserService {
    checkIfIdExists(id: string): boolean{
    /*
      CastError: Cast to ObjectId failed for value "61d6c3d7d59919c6bb46317h" (type string) at path "_id" for model "user"
        at model.Query.exec (/Users/avitalvainer/Desktop/workspace/crud-express-mongo/node_modules/mongoose/lib/query.js:4498:21)
        at model.Query.Query.then (/Users/avitalvainer/Desktop/workspace/crud-express-mongo/node_modules/mongoose/lib/query.js:4592:15)
    */
  
        return mongoose.isValidObjectId(id);
    }

    async getAllUsers() {
        const users = await user_model.find().select("_id first_name last_name email phone"); 
        return users;
    }

    async getById(id: string): Promise<IDbUser | undefined | null> {
        if(this.checkIfIdExists(id) === true) {
            const user = await user_model.findById(id);
            return user;
        }
        else {
            return undefined;
        }
    }
    
    async createUser(newUser: ICreateUser) {
        const isValid = await validateUserData(newUser, CREATE_USER); // in creation - we want all the user fields to be required, and in update - we want all the user fields to be optional

        if(isValid) {
          const user = await user_model.create(newUser);
          return user;
        }
        else return undefined;
    }

    async updateUser(id: string, updatedData: IUpdateUser) {
        const isValid = await validateUserData(updatedData, UPDATE_USER);
        if(isValid) {
          const user = await user_model.findByIdAndUpdate(id, updatedData, {new: true, upsert: false });
          return user;
        }
        else return undefined;
    }

    async deleteUser(id: string) {
        const user = await user_model.findByIdAndRemove(id);
        return user;
    }

    async paginate(pageNum: number, usersPerPage: number) {
        const users = await user_model.find({}).skip(pageNum * usersPerPage).limit(usersPerPage);
        return users;
    }
}


const instance = new UserService();
export default instance;