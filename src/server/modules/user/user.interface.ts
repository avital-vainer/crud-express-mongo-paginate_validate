
export interface IUser {
    _id: string;
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
}

export type ICreateUser = Omit<IUser, "_id">;
export type IUpdateUser = Partial<IUser>;