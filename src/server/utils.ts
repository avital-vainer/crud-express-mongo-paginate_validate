/* eslint-disable @typescript-eslint/no-namespace */


export interface IErrorResponse {
    status: number;
    msg: string;
    stack?: string;
}


// export interface IUser {
//     id: string;
//     firstName: string;
//     lastName: string;
//     age: number;
//     city: string;
// }

declare global {
    namespace Express {
        interface Request {
            id: string;
        }
    }
}

export const reqLogFilePath = `${process.cwd()}/requests.log`;
export const errorLogFilePath = `${process.cwd()}/errors.log`;

// export const generateUniqueId = () => Math.random().toString(36).slice(-5);